clear all;
clc;
close all


files=dir(['MATFiles/','*.mat']);

numberOfFiles = length(files);
loadfiles = 1;

stelWindow = 15*60/10;
twaWindow = 8*60*60/10;

if loadfiles == 1
    for lmn = 1:numberOfFiles
        
        filename = ['MATFiles/',files(lmn).name];
        load(filename);
        
        
        
        stel = movmean(data.PMr,[stelWindow 0 ]);
        twa = movmean(data.PMr,[twaWindow 0 ]);
        data.PMr = (data.PMr./2.65).*1.65;
        figure(1)
        plot(data.DateTime,data.PMr,'r')
        hold on
        
        figure(2)
        p1 = scatter(data.DateTime,data.PMr,3,'.','r' ,'MarkerFaceAlpha',.1,'MarkerEdgeAlpha',.1);
        hold on
        p2 = plot(data.DateTime,stel,'k');
        p3 = plot(data.DateTime,twa,'b');
        
        figure(3)
        p4 = scatter(data.DateTime,data.PMr,3,'.','r' ,'MarkerFaceAlpha',.1,'MarkerEdgeAlpha',.1);
        hold on
        p5 = plot(data.DateTime,stel,'k');
        p6 = plot(data.DateTime,twa,'b');
        
        
    end
end


figure(1)
grid('on')
xlim([datetime(2018,7,28,0,0,0),datetime(2018,8,22,0,0,0)])
ylim([0,2000])
ylabel('Respirable fraction [{\mu}g/m{^3}]')
xlabel('Date/Time')
title('Live Trolex car park data')
set(gca,'fontsize',15)
print('RawData.jpeg','-djpeg')

figure(2)
grid('on')
xlim([datetime(2018,7,28,0,0,0),datetime(2018,8,22,0,0,0)])
ylim([0,500])
legend([p2 p3],{'15 Min STEL','8 Hour TWA'});
ylabel('Respirable fraction [{\mu}g/m{^3}]')
xlabel('Date/Time')
title('Averaged Trolex car park data')
set(gca,'fontsize',15)
print('Averages.jpeg','-djpeg')


figure(3)
grid('on')
xlim([datetime(2018,8,5,0,0,0),datetime(2018,8,8,0,0,0)])
ylim([0,200])
legend([p5 p6],{'15 Min STEL','8 Hour TWA'});
ylabel('Respirable fraction [{\mu}g/m{^3}]')
xlabel('Date/Time')
title('Typical day')
set(gca,'fontsize',15)
print('TypicalDay.jpeg','-djpeg')


figure(3)
grid('on')
xlim([datetime(2018,8,5,0,0,0),datetime(2018,8,6,0,0,0)])
ylim([0,200])
legend([p5 p6],{'15 Min STEL','8 Hour TWA'});
ylabel('Respirable fraction [{\mu}g/m{^3}]')
xlabel('Date/Time')
title('Typical day')
set(gca,'fontsize',15)
print('SingleDay.jpeg','-djpeg')