clear all;
clc;


path = 'CSVFiles/';
files = dir([path,'*.csv']);
numberOfFiles = length(files);


for lmn = 1:numberOfFiles
    CSVFile = [path,files(lmn).name];
    data = importfile_22082018(CSVFile);
    matName = ['MATFiles/',files(lmn).name(1:8)];
    save(matName,'data')
end

    