# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 16:13:53 2018
Trolex car park data taken from OPC 176281701
expected failure due to low SFR values

@author: mattj
"""
#%reset -f

import datetime
import matplotlib.pyplot as plt 

date = []
sfr = []

date.append(datetime.date(2018,5,15))
sfr.append(1.473839337)

date.append(datetime.date(2018,5,21))
sfr.append(1.56497561)

date.append(datetime.date(2018,6,12))
sfr.append(0.780566952)

date.append(datetime.date(2018,6,13))
sfr.append(1.14076034)

date.append(datetime.date(2018,4,19))
sfr.append(4.194193154)

date.append(datetime.date(2018,4,20))
sfr.append(4.412138403)

date.append(datetime.date(2018,4,21))
sfr.append(4.230962636)

date.append(datetime.date(2018,4,22))
sfr.append(4.488317458)

date.append(datetime.date(2018,4,23))
sfr.append(4.488317458)

date.append(datetime.date(2018,4,24))
sfr.append(3.956354661)





plt.figure(1)
plt.clf()
plt.scatter(date,sfr,c='r')
plt.grid('on')
plt.xlabel('Date',fontsize=15)
plt.ylabel('SFR [ml/s]',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.show()
plt.savefig('SFR Trolex car park OPC _ 1.png')

