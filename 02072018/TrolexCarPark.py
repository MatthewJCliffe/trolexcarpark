# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 12:16:34 2018

@author: mattj
"""
import os 
import numpy as np
from glob import glob
from dateutil.parser import parse
import matplotlib.pyplot as plt

def LoadFile(fname,path):
    os.chdir(path)
    
    
        
    #Load constants from file
    dstore = np.genfromtxt(fname,delimiter=',',skip_header=12,max_rows=4)
    volume = dstore[1,1:17]
    density = dstore[2,1:17]
    
    #load datetime 
    del dstore
    dstore = np.genfromtxt(fname,dtype=str,delimiter=',',skip_header=18,skip_footer=1,usecols=0)
    dt=[]
    for lmn in np.linspace(0,len(dstore)-1,len(dstore)):
        lmn = int(lmn)
        dt.append(parse(dstore[lmn]).time())
    #load data
    del dstore
    bindata = np.genfromtxt(fname,delimiter=',',skip_header=18,skip_footer=1,usecols=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16))
    sfr = np.genfromtxt(fname,delimiter=',',skip_header=18,skip_footer=1,usecols=(22))
    sampletime = np.genfromtxt(fname,delimiter=',',skip_header=18,skip_footer=1,usecols=(21))
    #Load precalculated PMr
    pmr = np.genfromtxt(fname,delimiter=',',skip_header=18,skip_footer=1,usecols=(24))
    
    
    
    return bindata, sfr, sampletime, dt, volume, density, fname, pmr

os.chdir(r'C:\Users\mattj\OneDrive - trolex\Experimental\Data\AirXD\Trolex\02072018')

files = [f for f in os.listdir() if f.endswith('.CSV')]

for lmn in np.linspace(0,len(files)-1,len(files)):
    
    lmn = int(lmn)
    file = files[lmn]
    
    