clear all;
clc;
close all;

matexists = 1;
if matexists == 0
    files = dir('*.csv');
    
    for lmn = 1:length(files)
        
        Data=ReadCSV(files(lmn).name);
        
        fname = [files(lmn).name(1:8),'.mat'];
        save(fname,'Data')
        
        
        
    end
end


if matexists == 1;
    files = dir('*.mat');
    for lmn = 1:length(files)
        load(files(lmn).name);
        PMr = Data.PMr;
        Time = Data.DateTime;
        TWA1 = Data.TWA15min.*1e3;
        TWA2 = Data.TWA8hr.*1e3;
        
        figure(1)
        cla
        scatter(Time,PMr,5,'.')
        hold on
        plot(Time,TWA1,'r')
        hold on
        plot(Time,TWA2,'k')
        grid('on')
        
        xlabel('Date / Time')
        ylabel('PM4.25 [{\mu}g/m{^3}]')
        legend('PMr','15 Min TWA','8 Hour TWA')
        
        fname = [files(lmn).name(1:8),'.png'];
        
        %print(fname,'-dpng')
        
        figure(2)
        scatter(Time,PMr,5,'g','.')
        hold on
        plot(Time,TWA1,'r')
        hold on
        plot(Time,TWA2,'k')
        grid('on')
        xlabel('Date / Time')
        ylabel('PM4.25 [{\mu}g/m{^3}]')
        legend('PMr','15 Min TWA','8 Hour TWA')
       % print('Alldata.png','-dpng')
        
        
    end
    
end

% if 1 == 1 
%     fname = '18042400.CSV'
%     Data = ReadCSV(fname);
%     
% end
