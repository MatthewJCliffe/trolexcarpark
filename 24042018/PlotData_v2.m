function varargout = PlotData_v2(varargin)
% PLOTDATA_V2 MATLAB code for PlotData_v2.fig
%      PLOTDATA_V2, by itself, creates a new PLOTDATA_V2 or raises the existing
%      singleton*.
%
%      H = PLOTDATA_V2 returns the handle to a new PLOTDATA_V2 or the handle to
%      the existing singleton*.
%
%      PLOTDATA_V2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTDATA_V2.M with the given input arguments.
%
%      PLOTDATA_V2('Property','Value',...) creates a new PLOTDATA_V2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlotData_v2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlotData_v2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlotData_v2

% Last Modified by GUIDE v2.5 25-Apr-2018 08:55:03

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @PlotData_v2_OpeningFcn, ...
    'gui_OutputFcn',  @PlotData_v2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PlotData_v2 is made visible.
function PlotData_v2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlotData_v2 (see VARARGIN)

% Choose default command line output for PlotData_v2

%%Populates drop down list with files in DIR.
handles.output = hObject;
Data = dir('*.mat');
popuplist = cell(1,length(Data));

for lmn = 1:length(Data)
    popuplist{1,lmn} = Data(lmn).name;
end

set(handles.selectFile,'string',popuplist);
%%

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PlotData_v2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PlotData_v2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadAll.
function loadAll_Callback(hObject, eventdata, handles)
% hObject    handle to loadAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of loadAll


% --- Executes on selection change in selectFile.
function selectFile_Callback(hObject, eventdata, handles)
% hObject    handle to selectFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectFile contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectFile


% --- Executes during object creation, after setting all properties.
function selectFile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotdata_v2.
function plotData_Callback(hObject, eventdata, handles)
% hObject    handle to plotdata_v2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
allCSV = get(handles.loadAll,'value');
customData = get(handles.customData,'value');
plotPmr = get(handles.plotPmr,'value');
plotTWA1 = get(handles.plotTWA1,'value');
plotTWA2 = get(handles.plotTWA2,'value');
if allCSV == 0
    
    if customData == 0
        idx = get(handles.selectFile,'value');
        fname = get(handles.selectFile,'string');
        load(fname{idx});
        PMr = Data.PMr;
        Time = Data.DateTime;
        TWA1 = Data.TWA15min.*1e3;
        TWA2 = Data.TWA8hr.*1e3;
        if plotPmr == 1
            plot(handles.mainPlot,Time,PMr,'r');
            grid('on')
            hold on
            
        end
        if plotTWA1 == 1
            plot(handles.mainPlot,Time,TWA1,'k');
            hold on
            
        end
        if plotTWA2 == 1
            plot(handles.mainPlot,Time,TWA2,'g');
            hold on
            
        end
        hold on
        grid('on')
        
    end
    
end


% --- Executes on button press in clearPlot.
function clearPlot_Callback(hObject, eventdata, handles)
% hObject    handle to clearPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
cla()


% --- Executes during object creation, after setting all properties.
function mainPlot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mainPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate mainPlot



function pmPlot_Callback(hObject, eventdata, handles)
% hObject    handle to pmPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pmPlot as text
%        str2double(get(hObject,'String')) returns contents of pmPlot as a double


% --- Executes during object creation, after setting all properties.
function pmPlot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pmPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function average1_Callback(hObject, eventdata, handles)
% hObject    handle to average1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of average1 as text
%        str2double(get(hObject,'String')) returns contents of average1 as a double


% --- Executes during object creation, after setting all properties.
function average1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to average1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function average2_Callback(hObject, eventdata, handles)
% hObject    handle to average2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of average2 as text
%        str2double(get(hObject,'String')) returns contents of average2 as a double


% --- Executes during object creation, after setting all properties.
function average2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to average2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in customData.
function customData_Callback(hObject, eventdata, handles)
% hObject    handle to customData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of customData


% --- Executes on button press in plotPmr.
function plotPmr_Callback(hObject, eventdata, handles)
% hObject    handle to plotPmr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotPmr


% --- Executes on button press in plotTWA1.
function plotTWA1_Callback(hObject, eventdata, handles)
% hObject    handle to plotTWA1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotTWA1


% --- Executes on button press in plotTWA2.
function plotTWA2_Callback(hObject, eventdata, handles)
% hObject    handle to plotTWA2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotTWA2
