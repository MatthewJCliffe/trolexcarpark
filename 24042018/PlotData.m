function varargout = PlotData(varargin)
% PLOTDATA MATLAB code for PlotData.fig
%      PLOTDATA, by itself, creates a new PLOTDATA or raises the existing
%      singleton*.
%
%      H = PLOTDATA returns the handle to a new PLOTDATA or the handle to
%      the existing singleton*.
%
%      PLOTDATA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTDATA.M with the given input arguments.
%
%      PLOTDATA('Property','Value',...) creates a new PLOTDATA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PlotData_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PlotData_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PlotData

% Last Modified by GUIDE v2.5 25-Apr-2018 08:20:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @PlotData_OpeningFcn, ...
    'gui_OutputFcn',  @PlotData_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PlotData is made visible.
function PlotData_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PlotData (see VARARGIN)

% Choose default command line output for PlotData

%%Populates drop down list with files in DIR.
handles.output = hObject;
Data = dir('*.mat');
popuplist = cell(1,length(Data));

for lmn = 1:length(Data)
    popuplist{1,lmn} = Data(lmn).name;
end

set(handles.selectFile,'string',popuplist);
%%

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PlotData wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PlotData_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadAll.
function loadAll_Callback(hObject, eventdata, handles)
% hObject    handle to loadAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of loadAll


% --- Executes on selection change in selectFile.
function selectFile_Callback(hObject, eventdata, handles)
% hObject    handle to selectFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectFile contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectFile


% --- Executes during object creation, after setting all properties.
function selectFile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to selectFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotData.
function plotData_Callback(hObject, eventdata, handles)
% hObject    handle to plotData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
allCSV = get(handles.loadAll,'value');
if allCSV == 0
    idx = get(handles.selectFile,'value');   
    fname = get(handles.selectFile,'string');
    load(fname{idx});
    PMr = Data.PMr;
    Time = Data.DateTime;
    TWA1 = Data.TWA15min.*1e3;
    TWA2 = Data.TWA8hr.*1e3;
    plot(handles.mainPlot,Time,PMr);
       
end
