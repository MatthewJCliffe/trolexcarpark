%Analyise the SFR for russian meterology
%Typical SFR = 220 mL/min = 220e-3L/min
%Typical FR = 1.2 L/min
%Assume linear conversion factor 1.2/220e-3= 5.4545

clear all
cd('C:\Users\mattj\OneDrive - trolex\Experimental\Data\AirXD\Trolex\24042018')
figure(1)
clf()
files = dir('*.mat');
prq=1;
for lmn = 1:length(files)
    load(files(lmn).name);
    Data.SFRmls(isnan(Data.SFRmls))=0;
    SFRmls = Data.SFRmls;
    SFRls = SFRmls/1e3;
    SFRlmin = SFRls.*60;
    FRlmin = SFRlmin.*5.4545;
    
    for ttt = 1:length(FRlmin)-1
        FR(prq) = FRlmin(ttt);
        prq = prq+1;
    end
    
    figure(1)
    P1 = plot(Data.DateTime,FRlmin,'k');
    hold on
    P2 = line([Data.DateTime(1),Data.DateTime(length(Data.DateTime)-1)],[1.2*1.05,1.2*1.05],'Color','red','linewidth',3);
    hold on
    P3 = line([Data.DateTime(1),Data.DateTime(length(Data.DateTime)-1)],[1.2*0.95,1.2*0.95],'Color','red','linewidth',3);
    hold on 
    P4 = line([Data.DateTime(1),Data.DateTime(length(Data.DateTime)-1)],[1.2,1.2],'Color','green','linewidth',3);
    
end



sd= std(FR)*3;
me = mean(FR);

sd/me*100



figure(1)
grid('on')
xlabel('Date / Time')
ylabel('Total flow rate [L/min]')
legend([P1, P2, P4],{'Measured flow rate',' + 5/-% limit' ,' Nominal 1.2 L/Min'})
xlim([datetime(2018,4,19,12,00,00),Data.DateTime(lmn-2)])
